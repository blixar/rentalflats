import React from 'react'
import hoistNonReactStatics from 'hoist-non-react-statics'

export const AppContext = React.createContext()

export function withAppContext(Component) {
  const EnhancedComponent = props => (
    (
      <AppContext.Consumer>
        {context => <Component {...props} context={context} />}
      </AppContext.Consumer>
    )
  )
  return hoistNonReactStatics(EnhancedComponent, Component)
}
