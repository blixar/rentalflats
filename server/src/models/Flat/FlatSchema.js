import mongoose from 'mongoose'

import { handleMongooseHookError } from 'errors/mongooseErrorHandler'

const FlatSchema = new mongoose.Schema({
	location: {
		type: String,
		required: [true, 'location required'],
	},
	squareMeters: {
		type: Number,
		required: [true, 'squareMeters is required'],
	},
	rooms: {
		type: Number,
		required: [true, 'rooms is required'],
	},
	nightRate: {
		type: Number,
		required: [true, 'nightRate is required'],
	},
	monthRate: {
		type: Number,
		required: [true, 'monthRate is required'],
	},
	rentedBy: { type: mongoose.Schema.ObjectId, ref: 'UserModel' },
})

FlatSchema.post(/(find.*)|save/, handleMongooseHookError)

export default FlatSchema
