import { createSwitchNavigator } from 'react-navigation'

import {SignedInNavigator} from './SignedInNavigator'
import Login from 'features/Login'

export const RootNavigator = createSwitchNavigator(
    {
        SignedIn: SignedInNavigator,
        Login,
    },
    {
        initialRouteName: 'Login',
    },
)
