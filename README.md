# rentalflats

## Running the app

### Locally
- Clone the repo
- `cd rentalflats/server`
- Create `.env` with this config:
```
PORT=8080
BASE_API=/api
MONGODB_URI=test:test123@ds131323.mlab.com:31323/rentalflats
```
- `npm i`
- `npm start`
- `cd ../client`
- Modify `src/api/base.js`. baseUrl should be: `'http://{local ip}:8080/api'`
- `npm i`
- `npm start`
- Scan QR code with Expo app

### With Expo App
- Open Expo app in your phone
- Scan the QR code from this link: https://expo.io/@blixar/rentalflats


### Downloading apk and installing it manually
- https://expo.io/builds/7b3acffe-c4cc-4fc8-9551-4566a83b1b3e

### users
 - user: mona@magnetico.com.ar  password: 1234
 - user: gabriel@magnetico.com.ar  password: 1234
