import React from 'react'

import { AppContext } from './src/context/AppContext'
import { RootNavigator } from './src/navigators/RootNavigator'

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loggedIn: false,
      user: null,
      login: this.login,
      logout: this.logout,
    }
  }

  login = (user) => {
    this.setState({ loggedIn: true, user })
  }

  logout = async () => {
    this.setState({ loggedIn: false, user: null })
  }

  render() {
    return (
      <AppContext.Provider value={this.state}>
          <RootNavigator />
      </AppContext.Provider>
    )
  }
}
