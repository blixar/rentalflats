import React from 'react'
import { DatePickerAndroid, View } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

class DatePicker extends React.Component {
  showAndroidCalendar = async () => {
    const {date, minDate, maxDate, onChange} = this.props
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        date: date || new Date(),
        ...(minDate ? {minDate} : {}),
        ...(maxDate ? {maxDate} : {})
      })
      if (action !== DatePickerAndroid.dismissedAction) {
        const selectedDate = new Date(year, month, day)
        onChange(selectedDate)
      }
    } catch ({code, message}) {
      console.error('Cannot open date picker', message);
    }
  }

  render() {
    const {children} = this.props
    return (
      <View>
        {children(this.showAndroidCalendar)}
      </View>
    )
  }
}

export default DatePicker



