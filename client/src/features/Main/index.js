import React from 'react'
import { View, StyleSheet } from 'react-native'

import { getFlats } from 'api/flats'
import { withAppContext } from 'context/AppContext'
import DatesToolbar from './DatesToolbar'
import FlatsList from './FlatsList'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

class Main extends React.Component {
  static navigationOptions = {
    title: 'Rentalflats',
  }

  constructor(props) {
    super(props)
    this.state = {
      flats: []
    }
  }

  onDatesSet = async (startDate, endDate) => {
    const {context} = this.props
    const flats = await getFlats(context.user.id, startDate, endDate)
    this.setState({flats})
  }

  onFlatRented = (rentedFlat) => {
    this.setState(prevState => ({flats: prevState.flats.map(flat => {
      if (flat.id === rentedFlat.id) return rentedFlat
      return flat
    })}))
  }

  render() {
    const {flats} = this.state
    const datesToolbarProps = {
      onDatesSet: this.onDatesSet,
    }
    return (
      <View style={styles.container}>
        <DatesToolbar {...datesToolbarProps}/>
        <FlatsList flats={flats} onFlatRented={this.onFlatRented}/>
      </View>
    )
  }
}

export default withAppContext(Main)