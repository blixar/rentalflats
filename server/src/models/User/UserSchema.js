import mongoose from 'mongoose'

import { handleMongooseHookError } from 'errors/mongooseErrorHandler'
import hashPassword from 'utils/hashPassword'

const IMMUTABLE_FIELDS = [
	'email',
]

function validateEmail(email) {
	const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	return regexp.test(email)
}

const UserSchema = new mongoose.Schema({
	email: {
		type: String,
		unique: true,
		required: [true, 'Mail is required'],
		validate: {
			validator: validateEmail,
			message: '{VALUE} is not a valid email',
		},
	},
	password: { type: String, required: [true, 'Password is required'], select: false },
	haveRented: { type: Boolean, default: false },
}, { toJson: { virtuals: true } })

UserSchema.virtual('rents', {
	ref: 'FlatModel',
	localField: '_id',
	foreignField: 'rentedBy',
})


UserSchema.pre('save', async function preSave() {
	if (!this.isModified('password')) return
	this.password = await hashPassword(this.password)
})

UserSchema.pre('findOneAndUpdate', async function preFindOneAndUpdate() {
	const update = this.getUpdate()
	IMMUTABLE_FIELDS.forEach(field => delete update[field])
	if (!update.password) return
	update.password = await hashPassword(update.password)
})

UserSchema.post(/(find.*)|save/, handleMongooseHookError)

export default UserSchema
