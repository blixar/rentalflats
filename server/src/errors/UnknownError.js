import { AbstractError } from './AbstractError'

export class UnknownError extends AbstractError {
	constructor(message, name = 'UnknownError') {
		const userMessage = 'There was a problem processing your request. Try again'
		super(name, 500, message, userMessage)
	}
}
