import React from 'react'
import { createStackNavigator } from 'react-navigation'

import Main from 'features/Main'
import RightToolbar from 'components/RightToolbar'

export const SignedInNavigator = createStackNavigator(
  {
    Main,
  },
  {
    navigationOptions: {
      headerRight: <RightToolbar/>,
      headerRightContainerStyle: {
        marginRight: 20,
      },
      headerStyle: {
        backgroundColor: 'dodgerblue',
      },
      headerTitleStyle: {
        color: 'white',
      },
    },
  },
)