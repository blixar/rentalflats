import { baseUrl } from './base'

const endpoint = '/auth'

export async function authorizeUser(email, password) {
  const response = await fetch(`${baseUrl}${endpoint}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({user: {email, password}}),
  })
  if (response.status !== 200) throw new Error(`Request failed. Status: ${response.status}`)
  return (await response.json()).user
}