import React from 'react'
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

import { rentFlat } from 'api/flats'
import { withAppContext } from 'context/AppContext'

const styles = StyleSheet.create({
  flatCard: {
    display: 'flex',
    flexDirection: 'column',
    width: '80%',
    marginTop: 10,
    marginBottom: 10,
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 4,
  },
  flatCardLocation: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 5,
  },
  flatCardButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '50%',
    alignSelf: 'center',
    backgroundColor:'dodgerblue',
    elevation: 4,
    borderRadius: 2,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    padding: 8,
    fontWeight: '500',
  },
  flatCardRow: {
    display: 'flex',
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-around',
  },
  flatCardPrice: {
    textAlign: 'center',
    padding: 10,
  },
})

export class FlatCard extends React.Component {
  rented = () => {
    const {flat, context} = this.props
    return flat.rentedBy === context.user.id
  }

  onRent = async () => {
    if (this.rented()) return
    const {flat, onFlatRented, context} = this.props
    const rentedFlat = await rentFlat(flat.id, context.user.id)
    onFlatRented(rentedFlat)
  }

  render() {
    const {flat} = this.props
    const rented = this.rented()
    return (
      <View style={styles.flatCard}>
        <Text style={styles.flatCardLocation}>{flat.location}</Text>
        <View style={styles.flatCardRow}><Text>{`Rooms: ${flat.rooms}`}</Text><Text>{`${flat.squareMeters} Sqm.`}</Text></View>
        {!rented && <Text style={styles.flatCardPrice}>{`${flat.nightRate.toFixed(2)} USD per night`}</Text>}
        {!rented && <Text style={styles.flatCardPrice}>{`${flat.monthRate.toFixed(2)} USD per month`}</Text>}
        <TouchableOpacity style={styles.flatCardButton} onPress={this.onRent} activeOpacity={0.8} testID={'RentFlatCardButton'}>
          <Text style={styles.buttonText}>{rented ? 'Rented' : 'Rent'}</Text>
          {rented && <Ionicons name='md-checkmark-circle' size={24} color='white'/>}
        </TouchableOpacity>
      </View>
    )
  }
}

export default withAppContext(FlatCard)