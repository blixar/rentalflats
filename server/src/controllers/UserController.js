import { Router } from 'express'

import { ResourceNotFoundError } from 'errors'
import UserService from 'services/UserService'
import { wrap } from 'utils/requestHandlerWrapper'

export const UserController = Router()

/**
 * Find user by id
 */
UserController.get('/:id', wrap(async (req, res) => {
	const user = await UserService.findUserById(req.params.id)
	if (!user) {
		throw new ResourceNotFoundError()
	}
	return res.status(200).json({ user })
}))

/**
 * Find all users
 */
UserController.get('/', wrap(async (req, res) => {
	const users = await UserService.findUsers()
	return res.status(200).json({ users })
}))

/**
 * Create user
 */
UserController.post('/', wrap(async (req, res) => {
	const user = await UserService.createUser(req.body.user)
	return res.status(200).json({ user })
}))

/**
 * Update user by id
 */
UserController.post('/:id', wrap(async (req, res) => {
	const user = await UserService.updateUser(req.params.id, req.body.user)
	if (!user) {
		throw new ResourceNotFoundError()
	}
	return res.status(200).json({ user })
}))

/**
 * Delete user by id
 */
UserController.delete('/:id', wrap(async (req, res) => {
	const user = await UserService.deleteUser(req.params.id)
	if (!user) {
		throw new ResourceNotFoundError()
	}
	return res.sendStatus(200)
}))
