import React from 'react'
import DatesToolbar from './DatesToolbar'

class DatesToolbarContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      startDate: null,
      endDate: null,
    }
  }

  onStartDateChange = (date) => {
    this.onDateChange('startDate', date)
  }

  onEndDateChange = (date) => {
    this.onDateChange('endDate', date)
  }

  onDateChange = (whichDate, date) => {
    this.setState({[whichDate]: date}, this.onDatesSet)
  }

  onDatesSet = () => {
    const {startDate, endDate} = this.state
    if (startDate && endDate) {
      this.props.onDatesSet(startDate, endDate)
    }
  }

  render() {
    const {startDate, endDate} = this.state
    return (
      <DatesToolbar
        startDate={startDate}
        endDate={endDate}
        onStartDateChange={this.onStartDateChange}
        onEndDateChange={this.onEndDateChange}/>
    )
  }
}

export default DatesToolbarContainer