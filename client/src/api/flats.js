import { baseUrl } from './base'

const endpoint = '/flats'

export async function getFlats(userId, startDate, endDate) {
  const response = await fetch(`${baseUrl}${endpoint}?userId=${userId}&startDate=${startDate}&endDate=${endDate}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
  if (response.status !== 200) throw new Error(`Request failed. Status: ${response.status}`)
  return (await response.json()).flats
}

export async function rentFlat(flatId, userId) {
  const response = await fetch(`${baseUrl}${endpoint}/${flatId}/rents`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({user: {id: userId}}),
  })
  if (response.status !== 200) throw new Error(`Request failed. Status: ${response.status}`)
  return (await response.json()).flat
}