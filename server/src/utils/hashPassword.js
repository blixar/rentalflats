import bcrypt from 'bcrypt'

const SALT_ROUNDS = 10

export default async function hashPassword(plainText) {
	return bcrypt.hash(plainText, SALT_ROUNDS)
}
