import FlatService from './FlatService'
import UserService from './UserService'

async function rentFlat(flatId, userId) {
	return Promise.all([
		FlatService.updateFlat(flatId, { rentedBy: userId }),
		UserService.updateUser(userId, { haveRented: true }),
	])
}

export default {
	rentFlat,
}
