import React from 'react'
import { Ionicons } from '@expo/vector-icons'
import { withNavigation } from 'react-navigation'
import { withAppContext } from 'context/AppContext'

export class RightToolbar extends React.Component {
  logout = () => {
    const {context, navigation} = this.props
    context.logout()
    navigation.navigate('Login')
  }

  render() {
    return <Ionicons name='md-exit' size={24} color='white' onPress={this.logout}/>
  }
}

export default withNavigation(withAppContext(RightToolbar))