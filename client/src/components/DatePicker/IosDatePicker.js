import React, { Fragment } from 'react'
import { DatePickerIOS, View, Modal, StyleSheet, Button } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.58)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  background: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalContent: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingTop: 8,
    paddingLeft: 8,
    paddingRight: 8,
    paddingBottom: 16,
    margin: 16
  },
})

class DatePicker extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isOpen: false,
      currentDate: props.date || new Date()
    }
  }

  showDatePicker = () => {
    this.setState({isOpen: true})
  }

  closeDatePicker = () => {
    this.setState({isOpen: false})
  }

  handleOnChangeDate = () => {
    const {onChange} = this.props
    const {currentDate} = this.state
    onChange(currentDate)
    this.closeDatePicker()
  }

  onCurrentDateChange = (currentDate) => {
    this.setState({currentDate})
  }

  render () {
    const {children, maxDate, minDate} = this.props
    const {isOpen, currentDate} = this.state
    return (
      <Fragment>
        <View>
          {children(this.showDatePicker)}
        </View>
        <Modal transparent style={{flex: 1}} visible={isOpen} onDismiss={this.closeDatePicker} onRequestClose={this.closeDatePicker} animationType='fade'>
          <View style={styles.container}>
            <View style={styles.background}>
              <View style={{flexDirection: 'row'}}>
                <View style={styles.modalContent}>
                  <DatePickerIOS
                    date={currentDate}
                    maximumDate={maxDate}
                    minimumDate={minDate}
                    onDateChange={this.onCurrentDateChange}
                  />
                  <View style={{flexDirection: 'row',justifyContent: 'space-around', backgroundColor: '#FFF'}}>
                    <Button title="Cancel" onPress={this.closeDatePicker}/>
                    <Button title="Ok" onPress={this.handleOnChangeDate}/>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </Fragment>
    )
  }
}

export default DatePicker


