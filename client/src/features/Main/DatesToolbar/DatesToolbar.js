import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import DatePicker from 'components/DatePicker'

import { Ionicons } from '@expo/vector-icons'

const styles = StyleSheet.create({
  datesToolbar: {
    padding: 10,
    backgroundColor: 'white',
  },
  datesTitle: {
    fontWeight: 'bold',
  },
  datesContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 15,
  },
  dateButton: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#eaeaea',
    padding: 10,
    borderRadius: 20,
  },
  selectedDateText: {
    lineHeight: 20,
    marginLeft: 10,
  },
  datePickerIOS: {
    position: 'absolute',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

class DatesToolbar extends React.Component {

  renderDatePicker = (date, onChange, minDate, maxDate) => {
    const dateButtonBaseProps = {
      activeOpacity: 0.6,
      style: styles.dateButton
    }
    const label = date ? date.toDateString() : 'Select Date'
    return (
    <DatePicker date={date} onChange={onChange} minDate={minDate} maxDate={maxDate}>
      {showDatePicker => (
        <TouchableOpacity {...dateButtonBaseProps} onPress={showDatePicker}>
          <Ionicons name='md-calendar' size={24} color='black'/>
          <Text style={styles.selectedDateText}>{label}</Text>
        </TouchableOpacity>
      )}
    </DatePicker>)
  }

  render () {
    const {startDate, endDate, onStartDateChange, onEndDateChange} = this.props
    return (
      <View>
        <View style={styles.datesToolbar}>
          <Text style={styles.datesTitle}>Select dates for your stay:</Text>
          <View style={styles.datesContainer}>
            {this.renderDatePicker(startDate, onStartDateChange, new Date(), endDate)}
            {this.renderDatePicker(endDate, onEndDateChange, startDate)}
          </View>
        </View>
      </View>
    )
  }
}

export default DatesToolbar




