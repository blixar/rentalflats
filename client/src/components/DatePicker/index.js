import React from 'react'
import {Platform} from 'react-native'
import AndroidDatePicker from './AndroidDatePicker'
import IosDatePicker from './IosDatePicker'

const defaultProps = {
  date: new Date(),
  minDate: new Date()
}

AndroidDatePicker.defaultProps = defaultProps
IosDatePicker.defaultProps = defaultProps

if (Platform.OS === 'ios') {
  module.exports = IosDatePicker
}

if (Platform.OS === 'android') {
  module.exports = AndroidDatePicker
}