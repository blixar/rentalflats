import bodyParser from 'body-parser'
import express from 'express'
import winston from 'winston'

import 'config'
import { errorHandler } from 'errors/errorHandler'
import { UserController } from 'controllers/UserController'
import AuthController from 'controllers/AuthController'
import { FlatController } from 'controllers/FlatController'

const app = express()

app.use(bodyParser.json())

const { BASE_API } = process.env

app.use(`${BASE_API}/auth`, AuthController)
app.use(`${BASE_API}/users`, UserController)
app.use(`${BASE_API}/flats`, FlatController)

app.use(errorHandler)

const { PORT } = process.env
app.listen(PORT, winston.info(`RentalFlats server listening on port ${PORT}`))
