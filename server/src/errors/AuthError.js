import { AbstractError } from './AbstractError'

export class AuthError extends AbstractError {
	constructor() {
		const message = 'email or password invalid'
		const userMessage = 'email or password invalid. Try again.'
		super('AuthError', 400, message, userMessage)
	}
}
