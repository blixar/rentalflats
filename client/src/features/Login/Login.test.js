import React from 'react'
import { mount } from 'enzyme'
import renderer from 'react-test-renderer'

import { Login } from './'

describe('Login component', () => {
  it('Login: renders correctly', () => {
    const tree = renderer.create(<Login/>).toJSON()
    expect(tree).toMatchSnapshot()
  })
  const loginWrapper = mount(<Login/>).find('Login')
  const login = loginWrapper.instance()
  describe('Login: render', () => {
    it('Login: renders an image background', () => {
      expect(loginWrapper.find('ImageBackground').exists()).toBe(true)
    })
    it('Login: renders email input', () => {
      expect(loginWrapper.findWhere(n => n.name() === 'TextInput' && n.props().placeholder === 'Email').exists()).toBe(true)
    })
    it('Login: renders password input', () => {
      expect(loginWrapper.findWhere(n => n.name() === 'TextInput' && n.props().placeholder === 'Password').exists()).toBe(true)
    })
  })
  describe('Login: state', () => {
    it('Login: calling setEmail sets the email in the state', () => {
      const email = 'hola@gmail.com'
      login.setEmail(email)
      expect(login.state.email).toEqual(email)
    })
    it('Login: calling setPassword sets the password in the state', () => {
      const password = '1234'
      login.setPassword(password)
      expect(login.state.password).toEqual(password)
    })
  })
})