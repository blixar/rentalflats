import React from 'react'
import { ScrollView, Text, StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

import FlatCard from './FlatCard'

const styles = StyleSheet.create({
  noFlatsText: {
    textAlign: 'center',
    color: 'darkgrey',
  },
  flatsContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
})

const FlatsList = (props) => {
  const {flats, onFlatRented} = props
  return (
    <ScrollView contentContainerStyle={styles.flatsContainer}>
      {flats.length ?
        flats.map(flat => <FlatCard key={flat.id} flat={flat} onFlatRented={onFlatRented}/>)
        : <Text style={styles.noFlatsText}>There are no flats available for these dates or you haven't selected any dates yet</Text>}
    </ScrollView>
  )
}

export default FlatsList