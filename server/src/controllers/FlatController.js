import { Router } from 'express'

import { ResourceNotFoundError } from 'errors'
import FlatService from 'services/FlatService'
import RentService from 'services/RentService'
import PriceService from 'services/PriceService'
import { wrap } from 'utils/requestHandlerWrapper'

export const FlatController = Router()

/**
 * Find flat by id
 */
FlatController.get('/:id', wrap(async (req, res) => {
	const flat = await FlatService.findFlatById(req.params.id)
	if (!flat) {
		throw new ResourceNotFoundError()
	}
	return res.status(200).json({ flat })
}))

/**
 * Find all flats
 */
FlatController.get('/', wrap(async (req, res) => {
	const { userId, startDate, endDate } = req.query
	const flats = await FlatService.findFlats()
	const flatsWithDiscounts = await PriceService.applyDiscounts(flats, userId, startDate, endDate)
	return res.status(200).json({ flats: flatsWithDiscounts })
}))

/**
 * Create flat
 */
FlatController.post('/', wrap(async (req, res) => {
	const flat = await FlatService.createFlat(req.body.flat)
	return res.status(200).json({ flat })
}))

/**
 * Update flat by id
 */
FlatController.post('/:id', wrap(async (req, res) => {
	const flat = await FlatService.updateFlat(req.params.id, req.body.flat)
	if (!flat) {
		throw new ResourceNotFoundError()
	}
	return res.status(200).json({ flat })
}))

/**
 * Delete flat by id
 */
FlatController.delete('/:id', wrap(async (req, res) => {
	const flat = await FlatService.deleteFlat(req.params.id)
	if (!flat) {
		throw new ResourceNotFoundError()
	}
	return res.sendStatus(200)
}))

/**
 * Rent a flat by id
 */
FlatController.post('/:id/rents', wrap(async (req, res) => {
	const { id } = req.params
	const { user: { id: userId } } = req.body
	const [flat, user] = await RentService.rentFlat(id, userId)
	return res.status(200).json({ flat, user })
}))
