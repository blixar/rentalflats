import User from 'models/User'

async function findUserById(id) {
	return User.findById(id)
}

async function findUserByMail(email, withPassword) {
	const select = withPassword ? '+password' : ''
	return User.findOne({ email }).select(select).exec()
}

async function updateUser(id, userPayload) {
	const options = {
		new: true,
		runValidators: true,
	}
	return User.findByIdAndUpdate(id, userPayload, options).exec()
}

async function createUser(userPayload) {
	const user = new User(userPayload)
	return user.save()
}

async function deleteUser(id) {
	return User.findByIdAndRemove(id).exec()
}

async function findUsers(conditions = {}) {
	return User.find(conditions).exec()
}

export default {
	findUserById,
	findUserByMail,
	findUsers,
	updateUser,
	createUser,
	deleteUser,
}
