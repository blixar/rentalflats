import Flat from 'models/Flat'

async function findFlatById(id) {
	return Flat.findById(id).exec()
}

async function updateFlat(id, flatPayload) {
	const options = {
		new: true,
		runValidators: true,
	}
	return Flat.findByIdAndUpdate(id, flatPayload, options).exec()
}

async function createFlat(flatPayload) {
	const flat = new Flat(flatPayload)
	return flat.save()
}

async function deleteFlat(id) {
	return Flat.findByIdAndRemove(id).exec()
}

async function findFlats(conditions = {}) {
	return Flat.find(conditions).exec()
}

export default {
	findFlatById,
	findFlats,
	updateFlat,
	createFlat,
	deleteFlat,
}
