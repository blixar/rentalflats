import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import { RightToolbar } from './'

describe('RightToolbar component', () => {
  const mockNavigation = {navigate: jest.fn()}
  const mockContext = {logout: jest.fn()}
  it('RightToolbar: renders correctly', () => {
    const tree = renderer.create(<RightToolbar navigation={mockNavigation} context={mockContext} />).toJSON()
    expect(tree).toMatchSnapshot()
  })
  describe('Logout Icon component', () => {
    const rightToolbarWrapper = shallow(<RightToolbar navigation={mockNavigation} context={mockContext} />)
    const iconWrapper = rightToolbarWrapper.find('Icon')
    it('Logout Icon: renders correctly', () => {
      expect(iconWrapper.exists()).toBe(true)
      expect(iconWrapper.props().name).toEqual('md-exit')
    })
    it('Logout Icon: receives logout fn as onPress prop', () => {
      expect(iconWrapper.props().onPress).toEqual(rightToolbarWrapper.instance().logout)
    })
    it('RightToolbar: context logout is called when Icon is pressed', () => {
      iconWrapper.props().onPress()
      expect(mockContext.logout).toHaveBeenCalled()
    })
  })
})
