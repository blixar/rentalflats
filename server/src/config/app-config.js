import winston from 'winston'
import dotenv from 'dotenv'

dotenv.config()

winston.add(winston.transports.File, { filename: 'logs/rentalFlats-server.log', level: 'debug' })
