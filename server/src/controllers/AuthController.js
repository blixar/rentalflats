import { Router } from 'express'

import AuthService from 'services/AuthService'
import { wrap } from 'utils/requestHandlerWrapper'

const AuthController = Router()

/**
 * Authenticate user
 */
AuthController.post('/', wrap(async (req, res) => {
	const { email, password } = req.body.user
	const authorization = await AuthService.authenticate(email, password)
	return res.status(200).json(authorization)
}))

export default AuthController
