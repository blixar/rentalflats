import React from 'react'
import { Button, Text, ImageBackground, StyleSheet, View, TextInput } from 'react-native'

import { authorizeUser } from 'api/auth'
import {withAppContext} from 'context/AppContext'

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
  },
  alpha: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    backgroundColor: '#000',
    opacity: 0.45,
  },
  container: {
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    paddingTop: 100,
    paddingRight: 35,
    paddingBottom: 100,
    paddingLeft: 35,
  },
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: 50,
  },
  inputsContainer: {
    backgroundColor: 'white',
    padding: 30,
    opacity: 0.9,
    borderRadius: 5,
  },
  input: {
    padding: 5,
  },
})


export class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      authFailed: false,
    }
  }

  setEmail = (email) => {
    this.setState({email})
  }

  setPassword = (password) => {
    this.setState({password})
  }

  login = async () => {
    try {
      const {email, password} = this.state
      const user = await authorizeUser(email, password)
      const {navigation, context} = this.props
      context.login(user)
      navigation.navigate('Main')
    } catch (e) {
      console.error(e)
      this.setState({ authFailed: true, email: '', password: '' })
    }
  }

  render() {
    const {email, password, authFailed} = this.state
    const baseInputProps = {
      style: styles.input,
      placeholderTextColor: authFailed ? 'red' : 'grey',
      selectionColor: 'dodgerblue',
      underlineColorAndroid: 'dodgerblue',
      autoCapitalize: 'none',
    }
    return (
      <ImageBackground source={require('assets/login-background.jpg')} style={styles.image}>
        <View style={styles.alpha} />
        <View style={styles.container}>
          <Text style={styles.title}>Rentalflats</Text>
          <View style={styles.inputsContainer}>
            <TextInput {...baseInputProps} onChangeText={this.setEmail} value={email} placeholder='Email' />
            <TextInput {...baseInputProps} onChangeText={this.setPassword} value={password} placeholder='Password' secureTextEntry />
          </View>
          <Button color='dodgerblue' onPress={this.login} title='Login'/>
        </View>
      </ImageBackground>
    )
  }
}

export default withAppContext(Login)
