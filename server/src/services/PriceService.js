import moment from 'moment'
import UserService from 'services/UserService'

function userDiscount(total) {
	return total * 0.95
}

function gt4DaysDiscount(total) {
	return total * 0.95
}

function gt2WeeksDiscount(total) {
	return total * 0.85
}

async function getDiscounts(userId, startDate, endDate) {
	const discounts = []
	const user = await UserService.findUserById(userId)
	const dayDiff = moment(endDate).diff(moment(startDate), 'days')
	if (user.haveRented) discounts.push(userDiscount)
	if (dayDiff > 14) {
		discounts.push(gt2WeeksDiscount)
		return discounts
	}
	if (dayDiff > 4) discounts.push(gt4DaysDiscount)
	return discounts
}

export function composeDiscount(functions) {
	return value => functions.reduce((result, func) => func(result), value)
}

async function applyDiscounts(flats, userId, startDate, endDate) {
	const discounts = await getDiscounts(userId, startDate, endDate)
	const finalDiscount = composeDiscount(discounts)
	return flats.map(flat => ({
		...flat.toJSON(),
		nightRate: finalDiscount(flat.nightRate),
		monthRate: finalDiscount(flat.monthRate),
	}))
}

export default {
	applyDiscounts,
}
