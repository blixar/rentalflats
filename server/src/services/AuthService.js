import bcrypt from 'bcrypt'

import UserService from './UserService'
import { AuthError } from '../errors'

async function authenticate(email, password) {
	const user = await UserService.findUserByMail(email, true)
	if (!user) throw new AuthError()
	const match = await bcrypt.compare(password, user.password)
	if (!match) throw new AuthError()
	return { user }
}

export default {
	authenticate,
}
