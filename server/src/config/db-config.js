import mongoose from 'mongoose'
import winston from 'winston'
import toJson from '@meanie/mongoose-to-json'

const config = {
	useNewUrlParser: true,
}

const { MONGODB_URI } = process.env
const dbURI = `mongodb://${MONGODB_URI}`

if (!MONGODB_URI) {
	throw new Error('No MongoDB URI provided, please add the env variable MONGODB_URI')
}

mongoose.plugin(toJson)
mongoose.Promise = global.Promise
mongoose.connect(dbURI, config).catch(err => winston.error(err))
process.on('SIGINT', () => {
	mongoose.connection.close(() => {
		winston.info('Mongoose default connection disconnected through app termination')
		process.exit(0)
	})
})
