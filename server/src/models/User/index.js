import mongoose from 'mongoose'

import UserSchema from './UserSchema'

class UserModel extends mongoose.Model {
	constructor(user = {}) {
		super(user)
		this.email = user.email
		this.password = user.password
		this.haveRented = user.haveRented
	}
}

export default mongoose.model(UserModel, UserSchema, 'users')
