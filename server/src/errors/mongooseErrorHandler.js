/* eslint-disable no-unused-vars */
import winston from 'winston'
import ValidationError from 'mongoose/lib/error/validation'
import * as errors from '.'

const errorsWithValidation = { ...errors, ValidationError }

export async function handleMongooseHookError(err, doc, next) {
	winston.error(err)
	throw errorsWithValidation.hasOwnProperty(err.name) ? err : new errors.DBError()
}
