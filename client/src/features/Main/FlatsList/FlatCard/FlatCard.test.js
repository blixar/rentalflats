import React from 'react'
import { mount, shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import { FlatCard } from './'

describe('FlatCard component', () => {
  const mockFlat = {
    id: '8888',
    location: 'Villa Urquiza, Buenos Aires, Argentina',
    squareMeters: 150,
    rooms: 4,
    nightRate: 25,
    monthRate: 750,
    rentedBy: '1234'
  }
  const mockContext = {user: {id: '5555'}}
  it('FlatCard: renders correctly', () => {
    const tree = renderer.create(<FlatCard flat={mockFlat} context={mockContext}/>).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('FlatCard: rented fn is true if flat is rented by user', () => {
    const context = {user: {id: '1234'}}
    const flatCardWrapper = shallow(<FlatCard flat={mockFlat} context={context}/>)
    expect(flatCardWrapper.instance().rented()).toBe(true)
  })
  it('FlatCard: renders a rent button', () => {
    const flatCardWrapper = mount(<FlatCard flat={mockFlat} context={mockContext}/>)
    const touchableOpacityWrapper = flatCardWrapper.findWhere(n => n.name() === 'TouchableOpacity' && n.props().testID === 'RentFlatCardButton')
    expect(touchableOpacityWrapper.exists()).toBe(true)
  })
})
