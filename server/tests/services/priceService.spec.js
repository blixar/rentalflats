import { composeDiscount } from 'services/PriceService'

// const flats = [
//   {
//     location: 'someLocation',
//     rooms: 2,
//     squareMeters: 40,
//     nightRate: 100,
//     monthRate: 100,
//   },
//   {
//     location: 'someLocation2',
//     rooms: 2,
//     squareMeters: 40,
//     nightRate: 200,
//     monthRate: 200,
//   },
// ]

describe('PriceService', () => {
	describe('composeDiscounts', () => {
		it('should return a function that match the expected value', () => {
			const discount1 = value => value * 0.9
			const discount2 = value => value * 0.1
			const finalDiscount = composeDiscount([discount1, discount2])
			expect(finalDiscount(100)).toBe(9)
		})
	})
})
