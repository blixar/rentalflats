import mongoose from 'mongoose'

import FlatSchema from './FlatSchema'

class FlatModel extends mongoose.Model {
	constructor(flat = {}) {
		super(flat)
		this.location = flat.location
		this.squareMeters = flat.squareMeters
		this.rooms = flat.rooms
		this.nightRate = flat.nightRate
		this.monthRate = flat.monthRate
		this.rentedBy = flat.rentedBy
	}
}

export default mongoose.model(FlatModel, FlatSchema, 'flats')
